@file:Suppress("SpellCheckingInspection", "UnstableApiUsage")

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.quiltmc.loom") version "0.12.+"
	kotlin("jvm") version "1.7.20"
	java
}

version = "${project.version}+${libs.versions.minecraft.get()}"
val minecraft = libs.versions.minecraft.get()
val mappings = libs.versions.mappings.get()
val loader = libs.versions.loader.get()

repositories {
	mavenLocal()
	mavenCentral()
	maven( "https://jitpack.io" )
	maven( "https://repsy.io/mvn/enderzombi102/mc" )
	maven( "https://maven.terraformersmc.com/releases" )
	maven( "https://maven.quiltmc.org/repository/release" )
	maven( "https://maven.quiltmc.org/repository/snapshot" )
}

loom {
	shareRemapCaches.set( true )
	runtimeOnlyLog4j.set( true )
	runConfigs["client"].isIdeConfigGenerated = true
	runConfigs["server"].isIdeConfigGenerated = true
}

dependencies {
	minecraft( libs.minecraft )
	mappings( "org.quiltmc:quilt-mappings:$minecraft+build.$mappings:v2" )

	modImplementation( libs.bundles.mod.implementation )
	implementation(kotlin("stdlib-jdk8"))
	include( libs.enderlib )
}

tasks.withType<ProcessResources> {
	inputs.property( "loader_version"   , loader )
	inputs.property( "version"          , version )
	inputs.property( "minecraft_version", minecraft )
	filteringCharset = "UTF-8"

	filesMatching( "quilt.mod.json" ) {
		expand(
			"version"      to version,
			"group"        to project.group,
			"dependencies" to """
				{ "id": "quilt_loader", "versions": "$loader" },
				{ "id": "quilt_base", "versions": "*" },
				{ "id": "minecraft", "versions": ">=$minecraft" },
				{ "id": "java", "versions": ">=17" }""".trimIndent()
		)
		filter { it.substringBefore("///") }
	}
}

tasks.withType<JavaCompile> {
	options.encoding = "UTF-8"
	options.release.set( 17 )
}

tasks.withType<KotlinCompile> {
	kotlinOptions.jvmTarget = "17"
}

java {
	toolchain.languageVersion.set( JavaLanguageVersion.of( 17 ) )
	withSourcesJar()
}

tasks.withType<Jar> {
	archiveBaseName.set( project.name.toLowerCase() )
	filesMatching("LICENSE") {
		rename { "LICENSE_$archiveBaseName" }
	}
}
