@file:Suppress("UnstableApiUsage")
enableFeaturePreview("VERSION_CATALOGS")
pluginManagement {
	repositories {
		maven( "https://maven.quiltmc.org/repository/release" )
		maven( "https://maven.fabricmc.net/" )
		gradlePluginPortal()
	}
}
rootProject.name = "MineCursed"
