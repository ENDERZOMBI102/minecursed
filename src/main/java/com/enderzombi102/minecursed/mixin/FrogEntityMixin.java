package com.enderzombi102.minecursed.mixin;

import com.enderzombi102.minecursed.util.UtilKt;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.FrogEntity;
import net.minecraft.item.Items;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(FrogEntity.class)
public abstract class FrogEntityMixin extends AnimalEntity {
	protected FrogEntityMixin( EntityType<? extends AnimalEntity> entityType, World world ) {
		super( entityType, world );
	}

	@Inject( method = "tick", at = @At( "TAIL" ) )
	public void onTick( CallbackInfo ci ) {
		if ( UtilKt.allowFrogMagic( this.world ) ) {
			var entities = this.world.getEntitiesByClass(
				ItemEntity.class,
				Box.of( this.getPos(), 1.0, 1.0, 1.0 ),
				it -> it.getStack().isItemEqualIgnoreDamage( Items.SHIELD.getDefaultStack() )
			);
			if (! entities.isEmpty() ) {
				entities.get( 0 ).discard();
				this.convertTo( EntityType.TURTLE, true );
			}
		}
	}
}
