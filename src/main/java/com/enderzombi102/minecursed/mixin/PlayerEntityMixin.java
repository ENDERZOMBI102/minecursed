package com.enderzombi102.minecursed.mixin;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import java.util.Random;

import static com.enderzombi102.minecursed.util.UtilKt.translate;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin extends LivingEntity {
	@Shadow
	public abstract void wakeUp( boolean resetSleepTimer, boolean updateSleepingPlayers );

	@Shadow
	public abstract void sendMessage( Text message, boolean actionBar );

	@Unique
	private static final Random RANDOM = new Random();
	@Unique
	private boolean hasToWakeUpOnNoon = false;


	@SuppressWarnings("ConstantConditions")
	protected PlayerEntityMixin() {
		super( null, null );
	}

	@Redirect(
		method = "tick",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/entity/player/PlayerEntity;wakeUp(ZZ)V"
		)
	)
	@SuppressWarnings({ "AssignmentUsedAsCondition", "ConstantConditions" })
	public void onBeforeWakeUp( PlayerEntity instance, boolean resetSleepTimer, boolean updateSleepingPlayers ) {
		// FIXME: This doesn't work for some reason
		if ( this.hasToWakeUpOnNoon ) {
			if ( this.world.isNight() ) {
				this.wakeUp( false, true );
				this.hasToWakeUpOnNoon = false;
			}
		} else if ( hasToWakeUpOnNoon = ( RANDOM.nextInt(100) <= 100 ) )
			this.sendMessage( translate( "text", "depressedWakeUp" ), true );
		else
			this.wakeUp( resetSleepTimer, updateSleepingPlayers );
	}
}
