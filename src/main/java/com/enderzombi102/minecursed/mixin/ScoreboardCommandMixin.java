package com.enderzombi102.minecursed.mixin;

import net.minecraft.scoreboard.ScoreboardObjective;
import net.minecraft.server.command.ScoreboardCommand;
import net.minecraft.server.command.ServerCommandSource;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.util.Collection;

@Mixin(ScoreboardCommand.class)
public interface ScoreboardCommandMixin {
	@Invoker
	static int callExecuteSet( @NotNull ServerCommandSource source, @NotNull Collection<String> targets, @NotNull ScoreboardObjective objective, int score ) {
		throw new UnsupportedOperationException("Mixin invoked directly: This should not happen!");
	}

	@Invoker
	static int callExecuteAdd( @NotNull ServerCommandSource source, @NotNull Collection<String> targets, @NotNull ScoreboardObjective objective, int score ) {
		throw new UnsupportedOperationException("Mixin invoked directly: This should not happen!");
	}

	@Invoker
	static int callExecuteRemove( @NotNull ServerCommandSource source, @NotNull Collection<String> targets, @NotNull ScoreboardObjective objective, int score ) {
		throw new UnsupportedOperationException("Mixin invoked directly: This should not happen!");
	}
}
