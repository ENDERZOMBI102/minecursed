package com.enderzombi102.minecursed.mixin;

import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.server.command.EffectCommand;
import net.minecraft.server.command.ServerCommandSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(EffectCommand.class)
public class EffectCommandMixin {
	@Redirect(
		method = "register",
		at = @At(
			value = "INVOKE",
			ordinal = 0,
			target = "Lcom/mojang/brigadier/builder/LiteralArgumentBuilder;then(Lcom/mojang/brigadier/builder/ArgumentBuilder;)Lcom/mojang/brigadier/builder/ArgumentBuilder;"
		)
	)
	private static ArgumentBuilder<ServerCommandSource, LiteralArgumentBuilder<ServerCommandSource>> onRegister( LiteralArgumentBuilder<ServerCommandSource> instance, ArgumentBuilder<ServerCommandSource, LiteralArgumentBuilder<ServerCommandSource>> argumentBuilder ) {
		return instance.requires( value -> false );
	}
}
