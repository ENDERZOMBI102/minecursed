package com.enderzombi102.minecursed.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ConnectScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.TitleScreen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerWarningScreen;
import net.minecraft.client.network.ServerAddress;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MinecraftClient.class)
public class MinecraftClientMixin {
	@Shadow
	@Nullable
	public Screen currentScreen;

	@Inject( method = "setScreen", at = @At( "HEAD" ), cancellable = true )
	public void onSetScreen( @Nullable Screen screen, CallbackInfo info ) {
		if (! ( screen instanceof MultiplayerScreen ) )
			return;

		var entry = new ServerInfo( "HyPixel", "mc.hypixel.net", false );
		ConnectScreen.connect( currentScreen, ( (MinecraftClient) (Object) this ), ServerAddress.parse(entry.address), entry );
		info.cancel();
	}
}
