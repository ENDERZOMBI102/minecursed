package com.enderzombi102.minecursed.entity.ai.goal

import net.minecraft.entity.EntityType
import net.minecraft.entity.ItemEntity
import net.minecraft.entity.ai.goal.Goal
import net.minecraft.entity.mob.MobEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.util.math.Box

class TransformIntoByItemGoal<T>( private val entity: MobEntity, item: Item, private val type: EntityType<T>, private val keepEquipment: Boolean = true ) : Goal() where T : MobEntity {
	private val item: ItemStack = ItemStack( item )

	override fun canStart() =
		entity.world.getEntitiesByClass( ItemEntity::class.java, Box.of( entity.pos, 1.0, 1.0, 1.0 ) ) {
			it.stack.isItemEqualIgnoreDamage( item )
		}.isNotEmpty()

	override fun start() {
		entity.convertTo( type, keepEquipment )
	}
}