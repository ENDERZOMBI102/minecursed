package com.enderzombi102.minecursed

import com.enderzombi102.minecursed.registry.BlockEntityRegistry
import com.enderzombi102.minecursed.registry.BlockRegistry
import com.enderzombi102.minecursed.registry.EventListeners
import com.enderzombi102.minecursed.registry.ItemRegistry
import com.enderzombi102.minecursed.util.Const
import net.minecraft.item.ItemStack
import org.quiltmc.loader.api.ModContainer
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer
import org.quiltmc.qsl.item.group.api.QuiltItemGroup
import org.slf4j.LoggerFactory

class MineCursed : ModInitializer {
	override fun onInitialize( mod: ModContainer ) {
		Const.NAME = mod.metadata().name()
		Const.VERSION = mod.metadata().version().raw()

		BlockRegistry.register()
		ItemRegistry.register()
		BlockEntityRegistry.register()
		EventListeners.register()

		LOGGER.info( "MineCursed v${Const.VERSION} initialized, prepare yourself foolish mortal!" )
	}

	companion object {
		@JvmField
		val LOGGER = LoggerFactory.getLogger("MineCursed")
		val CURSED_TAB = QuiltItemGroup.createWithIcon( Const.getId("tab") ) {
			ItemStack( ItemRegistry["cataplant"] )
		}
	}
}
