package com.enderzombi102.minecursed.registry

import com.enderzombi102.minecursed.util.WorldScoped
import net.minecraft.entity.passive.FrogEntity
import org.quiltmc.qkl.wrapper.qsl.commands.onCommandRegistration
import org.quiltmc.qkl.wrapper.qsl.lifecycle.onServerWorldLoad
import org.quiltmc.qkl.wrapper.qsl.lifecycle.onServerWorldUnload
import org.quiltmc.qkl.wrapper.qsl.registerEvents

object EventListeners {
	fun register() {
		registerEvents {
			onCommandRegistration { _, _ ->
				CommandRegistry.register( this )
			}
			onServerWorldLoad( WorldScoped.Companion::onServerWorldStatusChange )
			onServerWorldUnload( WorldScoped.Companion::onServerWorldStatusChange )
		}
	}

	fun registerClient() = registerEvents {

	}
}
