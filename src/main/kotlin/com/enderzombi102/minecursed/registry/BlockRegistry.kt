package com.enderzombi102.minecursed.registry

import com.enderzombi102.minecursed.util.Const
import net.minecraft.block.Block
import net.minecraft.block.Blocks
import net.minecraft.util.registry.Registry

object BlockRegistry {
	private val BLOCKS = mapOf<String, Block>()

	fun register() {
		for ( ( key, value ) in BLOCKS )
			Registry.register( Registry.BLOCK, Const.getId( key ), value )
	}

	operator fun get( blockId: String ) = BLOCKS[blockId] ?: Blocks.AIR
}
