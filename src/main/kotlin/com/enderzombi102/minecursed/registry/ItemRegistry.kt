package com.enderzombi102.minecursed.registry

import com.enderzombi102.minecursed.util.Const
import com.enderzombi102.minecursed.MineCursed.Companion.CURSED_TAB
import net.minecraft.item.BlockItem
import net.minecraft.item.Item
import net.minecraft.item.Items
import net.minecraft.util.registry.Registry
import org.quiltmc.qsl.item.setting.api.QuiltItemSettings

object ItemRegistry {
	private val ITEMS = mapOf<String, Item>()

	fun register() {
		for ( ( key, value ) in ITEMS )
			Registry.register( Registry.ITEM, Const.getId( key ), value )
	}

	operator fun get( itemId: String ) = ITEMS[itemId] ?: Items.AIR

	private fun settings() = QuiltItemSettings().group(CURSED_TAB)

	private fun blockItem( id: String, settings: Item.Settings )= BlockItem( BlockRegistry[id], settings )
}
