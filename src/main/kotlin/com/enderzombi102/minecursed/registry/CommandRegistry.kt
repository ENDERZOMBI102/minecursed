package com.enderzombi102.minecursed.registry

import com.enderzombi102.minecursed.command.MilkCommand
import com.enderzombi102.minecursed.command.RandomCommand
import com.enderzombi102.minecursed.command.RandomCommand.rnd
import com.enderzombi102.minecursed.util.getChildRecursive
import com.mojang.brigadier.CommandDispatcher
import com.mojang.brigadier.arguments.IntegerArgumentType
import com.mojang.brigadier.context.CommandContext
import net.minecraft.server.command.CommandManager
import net.minecraft.server.command.CommandManager.literal
import net.minecraft.server.command.ServerCommandSource

object CommandRegistry {
	fun register( dispatcher: CommandDispatcher<ServerCommandSource> ) {
		dispatcher.register( MilkCommand.command )
		dispatcher.register( RandomCommand.command )
		// add random to the scoreboard command
		val executor: (CommandContext<ServerCommandSource>, Int) -> Int = { ctx, num ->
			val x: ScoreboardCommandMixin
			num
		}

		val com = literal("random")
			.then( CommandManager.argument("minimimnumber", IntegerArgumentType.integer() )
				.then( CommandManager.argument("maximumnumber", IntegerArgumentType.integer() )
					.executes {
						executor(
							it,
							rnd(it).rangeClosed(
								IntegerArgumentType.getInteger(it, "minimimnumber"),
								IntegerArgumentType.getInteger(it, "maximumnumber")
							)
						)
					}
				)
				.executes {
					executor(
						it,
						rnd(it).rangeClosed(
							IntegerArgumentType.getInteger(it, "minimimnumber"),
							Int.MAX_VALUE
						)
					)
				}
			)
			.executes { executor( it, rnd(it).nextInt() ) }

		listOf("add", "set", "remove").forEach {
			dispatcher.root
				.getChildRecursive("scoreboard.players.$it.targets.objective")
				.addChild( com.build() )
		}
	}
}
