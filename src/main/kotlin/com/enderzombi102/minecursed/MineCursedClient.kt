package com.enderzombi102.minecursed

import com.enderzombi102.minecursed.registry.EventListeners
import org.quiltmc.loader.api.ModContainer
import org.quiltmc.qsl.base.api.entrypoint.client.ClientModInitializer

class MineCursedClient : ClientModInitializer {
	override fun onInitializeClient( mod: ModContainer ) = EventListeners.registerClient()
}
