package com.enderzombi102.minecursed.command

import com.enderzombi102.minecursed.util.WorldScoped
import com.mojang.brigadier.arguments.IntegerArgumentType.getInteger
import com.mojang.brigadier.arguments.IntegerArgumentType.integer
import net.minecraft.server.command.CommandManager.argument
import net.minecraft.server.command.CommandManager.literal
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.text.Text
import org.quiltmc.qkl.wrapper.minecraft.brigadier.util.player
import org.quiltmc.qkl.wrapper.minecraft.brigadier.util.sendFeedback

object RandomCommand : CommandObject<ServerCommandSource> {
	private const val TRANSLATION = "command.minecursed.random"
	val rnd = WorldScoped { it.random.derive() }

	override val command = literal("random")
		.requires { it.hasPermissionLevel(2) }
		.then( argument( "minimimnumber", integer() )
			.then( argument( "maximumnumber", integer() )
				.executes {
					val num = rnd( it ).rangeClosed( getInteger( it, "minimimnumber" ), getInteger( it, "maximumnumber" ) )
					it.sendFeedback( Text.translatable( TRANSLATION, num ), false )
					num
				}
			)
			.executes {
				val num = rnd( it ).rangeClosed( getInteger( it, "minimimnumber" ), Int.MAX_VALUE )
				it.sendFeedback( Text.translatable( TRANSLATION, num ), false )
				num
			}
		)
		.executes {
			val num = rnd( it ).nextInt()
			it.sendFeedback( Text.translatable( TRANSLATION, num ), false )
			num
		}
}