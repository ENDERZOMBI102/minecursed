package com.enderzombi102.minecursed.command

import com.mojang.brigadier.Command
import com.mojang.brigadier.builder.LiteralArgumentBuilder
import com.mojang.brigadier.context.CommandContext

interface CommandObject<T> : Command<T> {
	val command: LiteralArgumentBuilder<T>

	override fun run( ctx: CommandContext<T> ): Int = 0
}
