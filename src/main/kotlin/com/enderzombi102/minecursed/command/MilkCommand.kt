package com.enderzombi102.minecursed.command

import com.enderzombi102.minecursed.util.buildArray
import com.mojang.brigadier.exceptions.CommandSyntaxException
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType
import net.minecraft.command.argument.EntityArgumentType
import net.minecraft.command.argument.StatusEffectArgumentType
import net.minecraft.entity.Entity
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.effect.StatusEffect
import net.minecraft.server.command.CommandManager.argument
import net.minecraft.server.command.CommandManager.literal
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.text.Text

object MilkCommand : CommandObject<ServerCommandSource> {
	private val CLEAR_EVERYTHING_FAILED_EXCEPTION = SimpleCommandExceptionType( Text.translatable("commands.effect.clear.everything.failed") )
	private val CLEAR_SPECIFIC_FAILED_EXCEPTION = SimpleCommandExceptionType( Text.translatable("commands.effect.clear.specific.failed") )
	override val command = literal("milk")
		.requires { it.hasPermissionLevel(2) }
		.executes { executeClear( it.source, listOf(it.source.entityOrThrow) ) }
		.then( argument("targets", EntityArgumentType.entities())
			.executes { executeClear( it.source, EntityArgumentType.getEntities(it, "targets") ) }
			.then( argument("effect", StatusEffectArgumentType.statusEffect())
				.executes {
					executeClear(
						it.source,
						EntityArgumentType.getEntities(it, "targets"),
						StatusEffectArgumentType.getStatusEffect(it, "effect")
					)
				}
			)
		)

	@Throws(CommandSyntaxException::class)
	private fun executeClear(
		source: ServerCommandSource,
		targets: Collection<Entity>,
		effect: StatusEffect? = null
	): Int {
		var i = 0

		for (entity in targets)
			if (entity is LivingEntity)
				when {
					effect != null && entity.removeStatusEffect(effect) -> ++i
					effect == null && entity.clearStatusEffects() -> ++i
				}

		val ( end, param ) = when (i) {
			0 -> throw ( if (effect == null) CLEAR_EVERYTHING_FAILED_EXCEPTION else CLEAR_SPECIFIC_FAILED_EXCEPTION ).create()
			1 -> "single" to targets.iterator().next().displayName
			else -> "multiple" to Text.literal( targets.size.toString() )
		}
		val type = if (effect == null) "everything" else "specific"

		source.sendFeedback(
			Text.translatable(
				"commands.effect.clear.$type.success.${end}",
				*buildArray {
					if (effect != null)
						add(effect.name)
					add(param)
				}
			),
			true
		)
		return i
	}
}