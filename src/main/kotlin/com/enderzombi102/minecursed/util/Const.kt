package com.enderzombi102.minecursed.util

import net.minecraft.util.Identifier

object Const {
	lateinit var NAME: String
	lateinit var VERSION: String
	const val ID = "minecursed"

	fun getId( path: String ) = Identifier( ID, path )
}
