package com.enderzombi102.minecursed.util

import com.mojang.brigadier.context.CommandContext
import net.minecraft.server.MinecraftServer
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.registry.RegistryKey
import net.minecraft.world.World
import org.quiltmc.qkl.wrapper.minecraft.brigadier.util.world
import java.lang.ref.WeakReference
import java.util.LinkedList
import kotlin.collections.HashMap

/**
 * A class that provides a value based off the current world.
 *
 * Every world has its own scope, and it's automatically dropped and created on world unload/load.
 */
class WorldScoped<T>( private val func: (ServerWorld) -> T ) {
	private val scopes: MutableMap<RegistryKey<World>, T> = HashMap()

	init { worldScopes.add( WeakReference(this) ) }

	operator fun invoke( world: ServerWorld ): T = scopes.computeIfAbsent( world.registryKey ) { func( world ) }
	operator fun invoke( ctx: CommandContext<ServerCommandSource> ): T = scopes.computeIfAbsent( ctx.world.registryKey ) { func( ctx.world ) }

	companion object {
		private val worldScopes: MutableList<WeakReference<WorldScoped<*>>> = LinkedList()

		fun onServerWorldStatusChange( server: MinecraftServer, world: ServerWorld ) {
			worldScopes.asReversed().forEach {
				val value = it.get()
				if ( value == null )
					worldScopes.remove( it )
				else
					value.scopes.remove( world.registryKey )
			}
		}
	}
}
