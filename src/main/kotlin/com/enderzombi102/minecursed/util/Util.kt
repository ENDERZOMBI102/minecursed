package com.enderzombi102.minecursed.util

import com.mojang.brigadier.tree.CommandNode
import net.minecraft.client.MinecraftClient
import net.minecraft.server.integrated.IntegratedServer
import net.minecraft.server.world.ServerWorld
import net.minecraft.text.Text
import net.minecraft.world.World
import java.util.*
import kotlin.collections.ArrayList

inline fun <reified T> buildArray( func: MutableList<T>.() -> Unit ): Array<T> {
	val list = ArrayList<T>()
	func.invoke( list )
	return list.toTypedArray()
}

fun translate( type: String, path: String ) = Text.translatable( "$type.minecursed.$path" )
fun translate( type: String, path: String, vararg args: Any ) = Text.translatable( "$type.minecursed.$path", *args )

// allowed IF world is on dedicated OR is on integrated + lang is germanic
fun allowFrogMagic( world: World )=
	world is ServerWorld && !(
		world.server is IntegratedServer &&
		!MinecraftClient.getInstance().languageManager.language.code.lowercase( Locale.getDefault() ).contains( "de" )
	)

fun <S> CommandNode<S>.getChildRecursive(path: String ): CommandNode<S> = if ( path.isEmpty() )
	this
else
	this.getChild( path.substringBefore(".") ).getChildRecursive( path.substringAfter(".") )